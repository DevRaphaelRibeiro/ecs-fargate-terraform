  FROM ruby:2.5.9
  RUN apt-get update && apt-get install -y nodejs
  WORKDIR /app
  COPY docker-rails/Gemfile* ./
  RUN bundle install
  COPY ./docker-rails .
  EXPOSE 3000
  CMD ["rails", "server", "-b", "0.0.0.0"]